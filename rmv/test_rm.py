# -*- coding: utf-8 -*-

import os
import unittest
import rmv.rm as rm
import tempfile
import rmv.trash as tr

class TestRm(unittest.TestCase):

	def setUp(self):
		
		self.test_trash = tempfile.mkdtemp()
		self.trash = tr.Trash(False, False, False, False, False)
		self.trash.path = self.test_trash

	def test_rmfile(self):
		ok, i = True, 0
		
		while i < 10:
			try:
				with tempfile.NamedTemporaryFile() as tmp:
					rm.rmfile(tmp.name, False, False, False)
				ok = ok and not os.path.exists(tmp.name)
			except:
				pass
			i += 1

		self.assertFalse(not ok, 'ERROR')


	def test_rmdir(self):
		ok, i = True, 0
		
		while i < 10:
			directory = tempfile.mkdtemp()
			rm.rmdir(directory, False, False, False)
			ok = ok and not os.path.exists(directory)
			i += 1

		self.assertFalse(not ok, 'ERROR')	


	def test_rmregex(self):
		ok = True

		directory = tempfile.mkdtemp()
		sub_directory = tempfile.mkdtemp(dir=directory)

		try:
			with tempfile.NamedTemporaryFile(dir=directory) as file_1:
				rm.rmregex(os.path.join(directory, '.'), False, False, False)
				ok = ok and not os.path.exists(file_1.name)

			with tempfile.NamedTemporaryFile(prefix = '1', dir=directory) as file_2:
				rm.rmregex(os.path.join(directory, '[0-9]'), False, False, False)
				ok = ok and not os.path.exists(file_2.name)

			with tempfile.NamedTemporaryFile(prefix = '1', dir=sub_directory) as file_3:
				rm.rmregex(os.path.join(directory, '\w+'), False, False, False)
				ok = ok and not os.path.exists(file_3.name)

		except:
			pass

		self.assertFalse(not ok, 'ERROR')


	def test_cleardir(self):
		ok = True

		directory = tempfile.mkdtemp()

		try:
			with tempfile.NamedTemporaryFile(dir = directory) as file:
				rm.cleardir(directory, False)
				ok = ok and not os.path.exists(file.name)
		except:
			pass

		self.assertFalse(not ok, 'ERROR')


	def test_rmfile_trash(self):
		ok, i = True, 0
		while i < 10:
			try:
				with tempfile.NamedTemporaryFile() as tmp:
					self.trash.get_file(tmp.name)
					ok = ok and not os.path.exists(tmp.name)
			except:
				pass
			i += 1

		self.assertFalse(not ok, 'ERROR')


	def test_rmdir_trash(self):
		ok, i = True, 0
		while i < 10:
			directory = tempfile.mkdtemp()
			self.trash.get_dir(directory)
			ok = ok and not os.path.exists(directory)
			i += 1

		self.assertFalse(not ok, 'ERROR')


	def test_trash_rmregex(self):
		ok = True

		directory = tempfile.mkdtemp()
		sub_directory = tempfile.mkdtemp(dir=directory)

		try:
			with tempfile.NamedTemporaryFile(dir=directory) as file_1:
				trash.get_files_reg(os.path.join(directory, '.'))
				ok = ok and not os.path.exists(file_1.name)

			with tempfile.NamedTemporaryFile(prefix = '1', dir=directory) as file_2:
				trash.get_files_reg(os.path.join(directory, '[0-9]'))
				ok = ok and not os.path.exists(file_2.name)

			with tempfile.NamedTemporaryFile(prefix = '1', dir=sub_directory) as file_3:
				trash.get_files_reg(os.path.join(directory, '\w+'))
				ok = ok and not os.path.exists(file_3.name)

		except:
			pass

		self.assertFalse(not ok, 'ERROR')
		

if __name__ == '__main__':
	unittest.main()