#!/usr/bin/env python 
# -*- coding: utf-8 -*-

import os
import re
import logging
import rmv.ask_user as ask
import rmv.exceptions2 as exceptions2

logger = logging.getLogger('application.rm')

# Removing a file
def rmfile(source, interactive, force, dryrun):
	name = os.path.basename(source)
	if (interactive and not force and not
		ask.AskUser.ask_user("rm: do you want to remove the file \'%s\' ?", name)):
		return

	if not os.access(source, os.W_OK) and not os.path.islink(source):
		logger.error('NotAccessError: no access rights for file \'%s\'', name)
		if not force:
			raise exceptions2.NotAccessError()
		return 

	if not dryrun:
		os.remove(source)

	logger.info("file \'%s\' was removed successfully", name)



# Replacing a file
def rpfile(source, destination):
	if os.path.exists(destination):
		destination = change_name(source, destination)
		os.rename(source, destination)
	else:
		os.rename(source, destination)
	endname = os.path.basename(destination)
	return endname


# Clear trash
def cleardir(source, dryrun):
	names = os.listdir(source)
	for name in names:
		fullname = os.path.join(source, name)
		try:
			if os.path.isfile(fullname):
				rmfile(fullname, False, True, dryrun)
			elif os.path.isdir(fullname):
				rmdir(fullname, False, True, dryrun)
		except:
			continue


def rmlink(source, interactive, force, dryrun, symlink):
	name = os.path.basename(source)
	if not symlink:
		rmfile(source, interactive, force, dryrun)
		return

	realpath = os.path.realpath(source)

	if os.path.isfile(realpath):
		rmfile(realpath, interactive, force, dryrun)
	elif os.path.isdir(realpath):
		rmdir(realpath, interactive, force, dryrun)
	else:
		logger.error('SymlinkError: symlink \'%s\' is not active', name)
		if not force:
			raise exceptions2.SymlinkError()


# Changing the name of file or directory
def change_name(source, destination):
	id = str(os.stat(source)[1])
	basename = os.path.basename(source)
	name, ext = os.path.splitext(basename)[0], os.path.splitext(basename)[1]
	destination = os.path.dirname(destination) + '/' + name + '_' + id + ext
	return destination


def rmdir(source, interactive, force, dryrun):
	name = os.path.basename(source)
	if (interactive and not force and not 
		ask.AskUser.ask_user("rm: do you want to remove the directory \'%s\' ?" %name)):
		return

	if not os.access(source, os.W_OK):
		logger.error('NotAccessError: no access rights for directory \'%s\'', name)
		if not force:
			raise exceptions2.NotAccessError()
		return 

	if not dryrun:
		rmtree(source)

	logger.info("directory \'%s\' was removed successfully", name)
	

# Alternative for shutil.rmtree
def rmtree(source):
	names = os.listdir(source)
	for name in names:
		fullname = os.path.join(source, name)
		if os.path.isdir(fullname):
			rmtree(fullname)
		elif os.path.isfile(fullname):
			os.remove(fullname)
	os.rmdir(source)


# Replacing a folder
def rpdir(source, destination):
	if os.path.exists(destination):
		destination = change_name(source, destination)
		os.rename(source, destination)
	else:
		os.rename(source, destination)
	endname = os.path.basename(destination)
	return endname

# Removing all files that match a regular expression
def rmregex(source, interactive, force, dryrun):
	if not os.path.exists(os.path.dirname(source)):
		logger.error("SourceError: directory \'%s\' does not exist",
					 os.path.dirname(source))
		if not force:
			raise exceptions2.SourceError()
		return

	pattern = os.path.basename(source)
	find = False
	for root, dirs, files in os.walk(os.path.dirname(source)):
		for file in files:
			if re.findall(pattern, file):
				rmfile(os.path.join(root, file), interactive, force, dryrun)
				find = True

	if not find:
		logger.error("NotFoundError: nothing found with regular expression \'%s\'",
					 pattern)			
		if not force:
			raise exceptions2.NotFoundError()

# Finding all files that match a regular expression
def find_files_regex(source):
	print os.path.dirname(source)
	print os.path.basename(source)

	if not os.path.exists(os.path.dirname(source)):
		logger.error("SourceError: directory \'%s\' does not exist",
					 os.path.dirname(source))
		raise exceptions2.SourceError()

	pattern = os.path.basename(source)
	names = []
	for root, dirs, files in os.walk(os.path.dirname(source)):
		for file in files:
			if re.findall(pattern, file):
				names.append(os.path.join(root, file))

	if len(names) == 0:
		logger.error("NotFoundError: nothing found with regular expression \'%s\'",
					 pattern)
		raise exceptions2.NotFoundError()
	return names


def rmpolicy(source, policy, space, interactive, dryrun):
	if (interactive and not
	    ask.AskUser.ask_user("rm: not enough memory! do you want to free space with \'%s\' policy?" %policy)):
		exit(0)

	list = []
	for root, dirs, files in os.walk(source):
		for file in files:
			list.append(os.path.join(root, file))

	if policy == 'time':
		list = sorted(list, key=os.path.getctime)
	elif policy == 'big':
		list = sorted(list, key=os.path.getsize, reverse=True)
	elif policy == 'full':
		cleardir(source, dryrun)
		logger.info("in the trash was cleaned %f mb with \'%s\' policy, ", space, policy)
		return

	size = 0
	for item in list:
		if os.path.basename(item) == 'content.json':
			continue

		size += os.path.getsize(item) / 1000000.0
		try:
			rmfile(item, False, True, dryrun)
		except NotAccessError:
			continue
		if size >= space:
			break

	logger.info("in the trash was cleaned %f mb with \'%s\' policy, ", space, policy)
